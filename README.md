# hometask-8

## Ответьте на вопросы

#### 1. Дайте определение переменным. Как можно их объявить?
> Ответ: Переменная - это структура для хранения данных, имеющая своё имя в программе.
#### 2. Перечислите все примитивные типы данных. В чём их отличие от ссылочных типов данных?
> Ответ: string, number, boolean, undefined, null, object. Когда мы присваиваем переменную примитивного типа другой переменной, значение копируется, тогда как при аналогичной 
операции со ссылочным типом данных значение второй переменной будет ссылаться на значение первой.
#### 3. Что такое массив? Как можно обратиться к элементу массива?
> Ответ: Массив - это объект, который хранит в себе некоторые данные, к которым можно обращаться по числовому индексу.
#### 4. Перечислите все из известных вам методов массива, при вызове которых возвращается новый массив.
> Ответ: concat, slice, filter, map.
#### 5. Объявите переменную типа `object`. В объекте должны лежать поля `name` и `age`.
> Ответ: var object = {
	name: 'Some name', age: 'Some age'};

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `tasks.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
